<?php
add_action( 'admin_menu', 'gurado_add_admin_menu' );
add_action( 'admin_init', 'gurado_settings_init' );

function gurado_add_admin_menu(  ) { 

	add_options_page( 'WC Gurado', 'WC Gurado', 'manage_options', 'wc_gurado', 'gurado_options_page' );

}


function gurado_settings_init(  ) { 

	register_setting( 'pluginPage', 'gurado_settings' );

	add_settings_section(
		'gurado_pluginPage_section', 
		__( '', 'site.com' ), 
		'gurado_settings_section_callback', 
		'pluginPage'
	);

	add_settings_field( 
		'gurado_text_field_0', 
		__( 'Consumer Key', 'site.com' ), 
		'gurado_text_field_0_render', 
		'pluginPage', 
		'gurado_pluginPage_section' 
	);

	add_settings_field( 
		'gurado_text_field_1', 
		__( 'Consumer Secret', 'site.com' ), 
		'gurado_text_field_1_render', 
		'pluginPage', 
		'gurado_pluginPage_section' 
	);


}

function gurado_text_field_0_render(  ) { 

	$options = get_option( 'gurado_settings' );
	?>
	<input type='text' name='gurado_settings[gurado_text_field_0]' value='<?php echo $options['gurado_text_field_0']; ?>'>
	<?php

}

function gurado_text_field_1_render(  ) { 

	$options = get_option( 'gurado_settings' );
	?>
	<input type='text' name='gurado_settings[gurado_text_field_1]' value='<?php echo $options['gurado_text_field_1']; ?>'>
	<?php

}

function gurado_settings_section_callback(  ) { 

	echo __( 'Gurado API options', 'site.com' );

}

function gurado_options_page(  ) { 

		?>
		<form action='options.php' method='post'>

			<h2>WC Gurado</h2>

			<?php
			settings_fields( 'pluginPage' );
			do_settings_sections( 'pluginPage' );
			submit_button();
			?>

		</form>
		<?php

}

?>
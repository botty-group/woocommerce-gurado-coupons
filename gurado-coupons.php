<?php

/**
 * Plugin Name: WooCommerce Gurado Coupons
 * Plugin URI: https://erictrenkel.com
 * Description: Schnittstelle für Gurado Coupons
 * Version: 1.0.0-dev
 * Author: Eric Trenkel
 * Author URI: https://erictrenkel.com
 * Developer: Eric Trenkel
 * Developer URI: https://erictrenkel.com
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * @package WooCommerce\Admin
 */

include "settings.php";

/**
 * Register the JS.
 */
function add_extension_register_script()
{
    if (!class_exists('Automattic\WooCommerce\Admin\Loader') || !\Automattic\WooCommerce\Admin\Loader::is_admin_or_embed_page()) {
        return;
    }

    $script_path       = '/build/index.js';
    $script_asset_path = dirname(__FILE__) . '/build/index.asset.php';
    $script_asset      = file_exists($script_asset_path)
        ? require($script_asset_path)
        : array('dependencies' => array(), 'version' => filemtime($script_path));
    $script_url = plugins_url($script_path, __FILE__);

    wp_register_script(
        'gurado-coupons',
        $script_url,
        $script_asset['dependencies'],
        $script_asset['version'],
        true
    );

    wp_register_style(
        'gurado-coupons',
        plugins_url('/build/index.css', __FILE__),
        // Add any dependencies styles may have, such as wp-components.
        array(),
        filemtime(dirname(__FILE__) . '/build/index.css')
    );

    wp_enqueue_script('gurado-coupons');
    wp_enqueue_style('gurado-coupons');
}


/**
 * Gurado API curl calls
 * @param string $method Method of the call. Either check or redeem.
 * @param string $voucher Code.
 * @param object $redeem Redeem object.
 * @return object curl result object.
 */
function guradoAPI($method, $voucher, $redeem = false)
{
    // DGVWH-Q3BTWTUG42
	$options = get_option( 'gurado_settings' );
    $curl = curl_init();

    switch ($method) {
        case "check":
            $url = "https://api.gurado.de/v1.0/vouchers/".$voucher;
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "{}",
                CURLOPT_HTTPHEADER => array(
                "x-gurado-consumer-key: ".$options['gurado_text_field_0'],
                "x-gurado-consumer-secret: ".$options['gurado_text_field_1']
                ),
            ));
            break;
        case "redeem":
            $url = "https://api.gurado.de/v1.0/vouchers/".$voucher."/redeem";
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => $redeem,
                CURLOPT_HTTPHEADER => array(
                  "content-type: application/json",
                  "x-gurado-consumer-key: ".$options['gurado_text_field_0'],
                  "x-gurado-consumer-secret: ".$options['gurado_text_field_1']
                ),
              ));
              
            break;
        default:
            break;
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

/**
 * Check coupon validity and return post response
 * @param string $coupon_code Coupon code.
 * @return object|false curl result object.
 */
function check_gurado_coupon($coupon_code)
{
    $response = guradoAPI("check", $coupon_code);
    if (json_decode($response, true)["messageStatus"]) {
        return false;
    } else {
        return $response;
    }
    return false;
}

/**
 * Handle Gurado Coupons and checks if we are in admin panel
 * @param false $false Just sets false.
 * @return object|false curl result object.
 */
add_filter('woocommerce_get_shop_coupon_data', 'handle_gurado_coupons', 10, 2);
function handle_gurado_coupons($false, $data)
{
    // Disable in admin panel
    if (is_admin()) {
        return $false;
    }
    create_temp_coupon($false, $data);
    return create_temp_coupon($false, $data);
}

/**
 * Create temporary coupon from Gurado
 * @param false $false Just sets false.
 * @return object|false Coupon settings.
 */
function create_temp_coupon($false, $data)
{
    $coupon_code_valid = false;
    $coupon_settings = null;

    $response = check_gurado_coupon($data);
    $balance = 0;
    
    if ($response) $balance = json_decode($response, true)["voucher"]["balance"];
    
    if ($balance > 0) {
        $coupon_code_valid = true;
    }
    
    if ($coupon_code_valid) {
        // Create a coupon with the properties you need
        $coupon_settings = array(
            'discount_type'              => 'fixed_cart', // 'fixed_cart', 'percent' or 'fixed_product'
            'amount'                     => $balance, // value or percentage.

            'individual_use'             => false,
            'product_ids'                => array(),
            'exclude_product_ids'        => array(),
            'usage_limit'                => '1',
            'usage_limit_per_user'       => '',
            'limit_usage_to_x_items'     => '',
            'usage_count'                => '0',
            'free_shipping'              => false,
            'product_categories'         => array(),
            'exclude_product_categories' => array(),
            'exclude_sale_items'         => false,
            'minimum_amount'             => '',
            'maximum_amount'             => '',
            'customer_email'             => array(),
        );

        return $coupon_settings;
    }
    return $false;
}

/**
 * Hooks & Filters
 */

// Apply only once
add_action('woocommerce_applied_coupon', 'action_applied_coupon');
function action_applied_coupon($coupon_code)
{
    if (check_gurado_coupon($coupon_code)) {
        error_log("Coupon '$coupon_code' has been applied. Mark as used.");
    }
}

add_action('woocommerce_removed_coupon', 'action_removed_coupon');
function action_removed_coupon($coupon_code)
{
    if (check_gurado_coupon($coupon_code)) {
        error_log("Coupon '$coupon_code' has been removed. Mark as UNused.");
    }
}

add_action('woocommerce_checkout_no_payment_needed_redirect', 'payment_successful_result', 10, 3);
add_action('woocommerce_payment_successful_result', 'payment_successful_result', 10, 3);
function payment_successful_result($result, $order_id)
{
    $order = wc_get_order($order_id);
    // Getting coupons
    $dp = (isset($filter['dp'])) ? intval($filter['dp']) : 2;
    foreach ($order->get_items('coupon') as $coupon_item_id => $coupon_item) {
        $order_data['coupon_lines'][] = array(
            'id' => $coupon_item_id,
            'code' => $coupon_item['name'],
            'amount' => wc_format_decimal($coupon_item['discount_amount'], $dp),
        );
        $code = $coupon_item['name'];
        $amount = wc_format_decimal($coupon_item['discount_amount'], $dp);

        // Redeem coupon
        guradoAPI("redeem", $code, "{\"amount\":".$amount."}");
    }
    
    if ( ! is_ajax() ) {
        // phpcs:ignore WordPress.Security.SafeRedirect.wp_redirect_wp_redirect
        wp_redirect( $result['redirect'] );
        exit;
    }

    wp_send_json( $result );
}

add_action('admin_enqueue_scripts', 'add_extension_register_script');